public class Semaforo {
    long switchRate;
    int nColoreCorrente = 0;
    String coloreCorrente = "Verde";
    boolean andando = false, stampa = false;

    /**
     * Costruttore di dichiarazione della classe Semaforo.
     * @param intervallo Intervallo di tempo tra un colore e l'altro.
     */
    public Semaforo(long intervallo){
        this.switchRate = intervallo;
        nomeColore();
    }

    /**
     * Costruttore di dichiarazione della classe Semaforo.
     * @param intervallo Intervallo di tempo tra un colore e l'altro.
     * @param coloreIniziale Con quale colore inizia per primo (0: Verde, 1: Giallo, 2: Rosso).
     */
    public Semaforo(long intervallo, int coloreIniziale){
        this.switchRate = intervallo;
        this.nColoreCorrente = coloreIniziale;
        nomeColore();
    }

    /**
     * Costruttore di dichiarazione della classe Semaforo.
     * @param intervallo Intervallo di tempo tra un colore e l'altro.
     * @param stampa Booleano che decide se stampa i colori quando cambiano.
     */
    public Semaforo(long intervallo, boolean stampa){
        this.switchRate = intervallo;
        this.stampa = stampa;
        nomeColore();
    }

    /**
     * Costruttore di dichiarazione della classe Semaforo.
     * @param intervallo Intervallo di tempo tra un colore e l'altro.
     * @param coloreIniziale Con quale colore inizia per primo (0: Verde, 1: Giallo, 2: Rosso).
     * @param stampa Booleano che decide se stampa i colori quando cambiano.
     */
    public Semaforo(long intervallo, int coloreIniziale, boolean stampa){
        this.switchRate = intervallo;
        this.nColoreCorrente = coloreIniziale;
        this.stampa = stampa;
        nomeColore();
    }

    /**
     * Aggiusta il nome del colore a seconda del numero impostato.
     */
    public void nomeColore(){
        switch (nColoreCorrente){
            case 0:coloreCorrente = "Verde"; break;
            case 1:coloreCorrente = "Giallo"; break;
            case 2:coloreCorrente = "Rosso"; break;
        }
    }

    /**
     * Aggiunge 1 al numero del colore. Se arriva al massimo torna a zero.
     */
    public void cambiaColore(){
        if(this.nColoreCorrente < 2){
            nColoreCorrente++;
        } else {
            nColoreCorrente = 0;
        }
        nomeColore();
        if(stampa){
            stampa();
        }
    }

    /**
     * Fai partire in automatico il semaforo.
     */
    public void vai() {
        if(stampa){
            stampa();
        }
        if(!andando){
            andando = true;
            while(andando){
                try {
                    Thread.sleep(switchRate*1000);
                }
                catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                cambiaColore();
            }
        }
    }

    /**
     * Ferma il semaforo.
     */
    public void ferma(){
        andando = false;
    }

    public void stampa(){
        System.out.println(coloreCorrente);
    }
}